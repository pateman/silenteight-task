package pl.pnusbaum.recruitmenttask.genderguess;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GenderguessApplication {

    public static void main(String[] args) {
        SpringApplication.run(GenderguessApplication.class, args);
    }

}
