package pl.pnusbaum.recruitmenttask.genderguess.engine;

public enum GuessResult {
    MALE, FEMALE, INCONCLUSIVE
}
