package pl.pnusbaum.recruitmenttask.genderguess.engine.impl;

import org.springframework.stereotype.Component;
import pl.pnusbaum.recruitmenttask.genderguess.engine.GuessEngine;
import pl.pnusbaum.recruitmenttask.genderguess.engine.GuessEngineException;
import pl.pnusbaum.recruitmenttask.genderguess.engine.GuessResult;
import pl.pnusbaum.recruitmenttask.genderguess.model.Gender;
import pl.pnusbaum.recruitmenttask.genderguess.model.repository.NamesRepository;
import pl.pnusbaum.recruitmenttask.genderguess.model.repository.RepositoryException;

import java.util.*;

@Component("repositoryBasedGuessEngine")
public class RepositoryBasedGuessEngine implements GuessEngine {

    private final NamesRepository namesRepository;

    public RepositoryBasedGuessEngine(NamesRepository namesRepository) {
        this.namesRepository = namesRepository;
    }

    @Override
    public GuessResult guessByFirstToken(String name) throws GuessEngineException {
        if (name == null) {
            throw new IllegalArgumentException("Name is required");
        }
        return genderToResult(findGenderForToken(tokenize(name).get(0)));
    }

    @Override
    public GuessResult guessByMajorityRule(String name) throws GuessEngineException {
        if (name == null) {
            throw new IllegalArgumentException("Name is required");
        }

        Map<GuessResult, Integer> guesses = new EnumMap<>(GuessResult.class);
        List<String> tokens = tokenize(name);

        for (String token : tokens) {
            GuessResult tokenResult = genderToResult(findGenderForToken(token));
            guesses.compute(tokenResult, (k, v) -> v == null ? 1 : v + 1);
        }

        Queue<Map.Entry<GuessResult, Integer>> queue = new PriorityQueue<>((a, b) -> a.getValue().equals(b.getValue())
                ? b.getKey().compareTo(a.getKey())
                : Integer.compare(b.getValue(), a.getValue()));
        guesses.entrySet().forEach(queue::offer);

        Map.Entry<GuessResult, Integer> topEntry = queue.poll();
        Map.Entry<GuessResult, Integer> secondTopEntry = queue.poll();

        if (topEntry == null) {
            return GuessResult.INCONCLUSIVE;
        } else if (secondTopEntry == null) {
            return topEntry.getKey();
        }

        return topEntry.getValue() > secondTopEntry.getValue() ? topEntry.getKey() : GuessResult.INCONCLUSIVE;
    }

    private List<String> tokenize(String value) {
        return Arrays.asList(value.split("\\s"));
    }

    private GuessResult genderToResult(Gender gender) {
        if (Gender.MALE == gender) {
            return GuessResult.MALE;
        } else if (Gender.FEMALE == gender) {
            return GuessResult.FEMALE;
        } else {
            return GuessResult.INCONCLUSIVE;
        }
    }

    private Gender findGenderForToken(String token) throws GuessEngineException {
        try {
            if (namesRepository.existsForGender(token, Gender.MALE)) {
                return Gender.MALE;
            } else if (namesRepository.existsForGender(token, Gender.FEMALE)) {
                return Gender.FEMALE;
            } else {
                return null;
            }
        } catch (RepositoryException e) {
            throw new GuessEngineException(e);
        }
    }
}
