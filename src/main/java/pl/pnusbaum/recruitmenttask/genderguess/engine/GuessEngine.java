package pl.pnusbaum.recruitmenttask.genderguess.engine;

public interface GuessEngine {

    GuessResult guessByFirstToken(String name) throws GuessEngineException;

    GuessResult guessByMajorityRule(String name) throws GuessEngineException;

}
