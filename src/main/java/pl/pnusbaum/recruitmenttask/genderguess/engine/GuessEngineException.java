package pl.pnusbaum.recruitmenttask.genderguess.engine;

public final class GuessEngineException extends Exception {

    public GuessEngineException(Throwable cause) {
        super(cause);
    }
}
