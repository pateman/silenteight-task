package pl.pnusbaum.recruitmenttask.genderguess.model.repository;

import pl.pnusbaum.recruitmenttask.genderguess.model.Gender;

import java.util.List;

public interface NamesRepository {

    List<String> findAllForGender(Gender gender) throws RepositoryException;

    boolean existsForGender(String name, Gender gender) throws RepositoryException;

}
