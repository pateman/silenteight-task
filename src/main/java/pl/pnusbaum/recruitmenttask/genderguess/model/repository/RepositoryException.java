package pl.pnusbaum.recruitmenttask.genderguess.model.repository;

public final class RepositoryException extends Exception {

    public RepositoryException(String message) {
        super(message);
    }

    public RepositoryException(String message, Throwable cause) {
        super(message, cause);
    }
}
