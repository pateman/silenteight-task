package pl.pnusbaum.recruitmenttask.genderguess.model.repository.impl;

import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;
import pl.pnusbaum.recruitmenttask.genderguess.model.Gender;
import pl.pnusbaum.recruitmenttask.genderguess.model.repository.NamesRepository;
import pl.pnusbaum.recruitmenttask.genderguess.model.repository.RepositoryException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Locale;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component("fileBasedNamesRepository")
public class FileBasedNamesRepository implements NamesRepository {

    @Override
    public List<String> findAllForGender(Gender gender) throws RepositoryException {
        if (gender == null) {
            throw new IllegalArgumentException("Unspecified gender");
        }

        return scanResource(determineResourceFile(gender), lines -> lines.collect(Collectors.toList()));
    }

    @Override
    public boolean existsForGender(String name, Gender gender) throws RepositoryException {
        if (name == null || gender == null) {
            throw new IllegalArgumentException("Both name and gender are required");
        }

        return scanResource(determineResourceFile(gender), lines -> lines.anyMatch(line -> line.equalsIgnoreCase(name)));
    }

    private String determineResourceFile(Gender gender) {
        return gender.toString().toLowerCase(Locale.ROOT) + "_names.txt";
    }

    private <T> T scanResource(String resourceFile, Function<Stream<String>, T> scanFunction) throws RepositoryException {
        try {
            try (InputStream inputStream = new ClassPathResource(resourceFile).getInputStream();
                 BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
                return scanFunction.apply(reader.lines());
            }
        } catch (IOException e) {
            throw new RepositoryException("Unable to access the file repository", e);
        }
    }
}
