package pl.pnusbaum.recruitmenttask.genderguess.model;

public enum Gender {
    MALE, FEMALE
}
