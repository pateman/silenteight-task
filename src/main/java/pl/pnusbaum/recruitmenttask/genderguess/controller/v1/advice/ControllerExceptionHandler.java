package pl.pnusbaum.recruitmenttask.genderguess.controller.v1.advice;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Collections;
import java.util.Map;

@RestControllerAdvice(basePackages = "pl.pnusbaum.recruitmenttask.genderguess.controller.v1")
public class ControllerExceptionHandler {

    @ExceptionHandler
    public ResponseEntity<Map<String, String>> handleException(Exception ex) {
        HttpStatus status;

        if (ex instanceof IllegalArgumentException) {
            status = HttpStatus.BAD_REQUEST;
        } else {
            status = HttpStatus.INTERNAL_SERVER_ERROR;
        }

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        return new ResponseEntity<>(Collections.singletonMap("message", ex.getMessage()), httpHeaders, status);
    }

}
