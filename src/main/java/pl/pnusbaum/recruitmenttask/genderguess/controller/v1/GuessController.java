package pl.pnusbaum.recruitmenttask.genderguess.controller.v1;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.pnusbaum.recruitmenttask.genderguess.engine.GuessEngine;
import pl.pnusbaum.recruitmenttask.genderguess.engine.GuessEngineException;
import pl.pnusbaum.recruitmenttask.genderguess.engine.GuessResult;
import pl.pnusbaum.recruitmenttask.genderguess.model.Gender;
import pl.pnusbaum.recruitmenttask.genderguess.model.repository.NamesRepository;
import pl.pnusbaum.recruitmenttask.genderguess.model.repository.RepositoryException;

import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@RestController
@RequestMapping("/v1/guess")
public class GuessController {

    private final NamesRepository namesRepository;
    private final GuessEngine guessEngine;

    public GuessController(NamesRepository namesRepository, GuessEngine guessEngine) {
        this.namesRepository = namesRepository;
        this.guessEngine = guessEngine;
    }

    @GetMapping("/tokens/{gender}")
    public List<String> getTokens(@PathVariable String gender) throws RepositoryException {
        return namesRepository.findAllForGender(Gender.valueOf(gender.toUpperCase(Locale.ROOT)));
    }

    @GetMapping("/{variant}/{name}")
    public Map<String, GuessResult> guess(@PathVariable String variant,
                                          @PathVariable String name) throws GuessEngineException {
        if ("single".equalsIgnoreCase(variant)) {
            return wrapResult(guessEngine.guessByFirstToken(name));
        } else if ("majority".equalsIgnoreCase(variant)) {
            return wrapResult(guessEngine.guessByMajorityRule(name));
        }

        throw new IllegalArgumentException("Unknown variant '" + variant + "'");
    }

    private Map<String, GuessResult> wrapResult(GuessResult result) {
        return Collections.singletonMap("result", result);
    }
}
