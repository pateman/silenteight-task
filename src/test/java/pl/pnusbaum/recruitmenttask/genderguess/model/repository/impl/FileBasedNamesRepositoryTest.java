package pl.pnusbaum.recruitmenttask.genderguess.model.repository.impl;

import org.junit.jupiter.api.Test;
import pl.pnusbaum.recruitmenttask.genderguess.model.Gender;
import pl.pnusbaum.recruitmenttask.genderguess.model.repository.RepositoryException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

class FileBasedNamesRepositoryTest {

    @Test
    void findAllForGender() throws RepositoryException {
        FileBasedNamesRepository repository = new FileBasedNamesRepository();

        assertThat(repository.findAllForGender(Gender.MALE)).containsExactlyInAnyOrder("Bogdan", "Marcin", "Patryk");
        assertThat(repository.findAllForGender(Gender.FEMALE)).containsExactlyInAnyOrder("Kasia", "Basia", "Ania");
    }

    @Test
    void existsForGender() throws RepositoryException {
        FileBasedNamesRepository repository = new FileBasedNamesRepository();

        assertThat(repository.existsForGender("Kasia", Gender.FEMALE)).isTrue();
        assertThat(repository.existsForGender("Patryk", Gender.FEMALE)).isFalse();
        assertThat(repository.existsForGender("Marcin", Gender.MALE)).isTrue();
        assertThat(repository.existsForGender("Basia", Gender.MALE)).isFalse();
    }

    @Test
    void shouldThrowIfMissingArgument() {
        assertThrows(IllegalArgumentException.class, () -> new FileBasedNamesRepository().findAllForGender(null));
        assertThrows(IllegalArgumentException.class, () -> new FileBasedNamesRepository().existsForGender("Kasia", null));
    }
}