package pl.pnusbaum.recruitmenttask.genderguess.engine.impl;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import pl.pnusbaum.recruitmenttask.genderguess.engine.GuessEngineException;
import pl.pnusbaum.recruitmenttask.genderguess.engine.GuessResult;
import pl.pnusbaum.recruitmenttask.genderguess.model.Gender;
import pl.pnusbaum.recruitmenttask.genderguess.model.repository.NamesRepository;
import pl.pnusbaum.recruitmenttask.genderguess.model.repository.RepositoryException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class RepositoryBasedGuessEngineTest {

    private static final String EMPTY_STRING = "";

    private NamesRepository namesRepository;

    @BeforeAll
    void setUp() throws RepositoryException {
        namesRepository = mock(NamesRepository.class);
        when(namesRepository.existsForGender("Jan", Gender.MALE)).thenReturn(true);
        when(namesRepository.existsForGender("Jan", Gender.FEMALE)).thenReturn(false);
        when(namesRepository.existsForGender("Zbigniew", Gender.MALE)).thenReturn(true);
        when(namesRepository.existsForGender("Zbigniew", Gender.FEMALE)).thenReturn(false);
        when(namesRepository.existsForGender("Maria", Gender.MALE)).thenReturn(false);
        when(namesRepository.existsForGender("Maria", Gender.FEMALE)).thenReturn(true);
        when(namesRepository.existsForGender("Rokita", Gender.MALE)).thenReturn(false);
        when(namesRepository.existsForGender("Rokita", Gender.FEMALE)).thenReturn(false);
    }

    @Test
    void guessByFirstToken() throws GuessEngineException {
        RepositoryBasedGuessEngine guessEngine = new RepositoryBasedGuessEngine(namesRepository);

        assertThat(guessEngine.guessByFirstToken("Rokita")).isEqualTo(GuessResult.INCONCLUSIVE);
        assertThat(guessEngine.guessByFirstToken("Maria")).isEqualTo(GuessResult.FEMALE);
        assertThat(guessEngine.guessByFirstToken("Maria Antonina")).isEqualTo(GuessResult.FEMALE);
        assertThat(guessEngine.guessByFirstToken("Jan Maria Rokita")).isEqualTo(GuessResult.MALE);
        assertThat(guessEngine.guessByFirstToken(EMPTY_STRING)).isEqualTo(GuessResult.INCONCLUSIVE);
    }

    @Test
    void guessByMajorityRule() throws GuessEngineException {
        RepositoryBasedGuessEngine guessEngine = new RepositoryBasedGuessEngine(namesRepository);

        assertThat(guessEngine.guessByMajorityRule("Jan Maria Rokita")).isEqualTo(GuessResult.INCONCLUSIVE);
        assertThat(guessEngine.guessByMajorityRule("Maria")).isEqualTo(GuessResult.FEMALE);
        assertThat(guessEngine.guessByMajorityRule("Zbigniew Maria Jan")).isEqualTo(GuessResult.MALE);
        assertThat(guessEngine.guessByMajorityRule("Rokita")).isEqualTo(GuessResult.INCONCLUSIVE);
        assertThat(guessEngine.guessByFirstToken(EMPTY_STRING)).isEqualTo(GuessResult.INCONCLUSIVE);
    }

    @Test
    void shouldThrowIfMissingArgument() {
        assertThrows(IllegalArgumentException.class, () -> new RepositoryBasedGuessEngine(namesRepository).guessByFirstToken(null));
        assertThrows(IllegalArgumentException.class, () -> new RepositoryBasedGuessEngine(namesRepository).guessByMajorityRule(null));
    }
}