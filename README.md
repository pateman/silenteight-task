# Silent Eight Recruitment Task
This is my recruitment task implementation for Silent Eight.

## Build and run with Docker
Very simple - in the project's root directory run:

`./gradlew build & docker build -t pnusbaum/genderguess .`

This will create the Docker image with the application. To run it, you can now call:

`docker run -d -p 80:8080 --name pnusbaum_genderguess pnusbaum/genderguess`

## Endpoints

### /v1/guess/tokens/(gender)
Returns all the available tokens for the given gender. Valid values for gender are: `male` and `female`.

### /v1/guess/(variant)/(name)
Runs the guessing algorithm using the provided variant against the given name. The available variants are:

1. `single` - only the first token is checked

2. `majority` - all tokens are checked and majority role is used

`name` is an URL-encoded string.