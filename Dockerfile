FROM adoptopenjdk/openjdk11:latest
COPY build/libs/genderguess*.jar app.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/app.jar"]